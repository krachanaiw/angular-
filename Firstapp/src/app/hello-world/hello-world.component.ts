import { Component, OnInit } from '@angular/core';
import {HttpClient} from "@angular/common/http";

@Component({
  selector: 'app-hello-world',
  templateUrl: './hello-world.component.html',
  styleUrls: ['./hello-world.component.css']
})
export class HelloWorldComponent implements OnInit {


  data: any;


  constructor(
    private http: HttpClient
  ) {
  }

  ngOnInit() {
    this.http.get(`http://127.0.0.1:8000/apiprofile/`).subscribe(response => {
      console.log(response);
      this.data = response;
    }
);


}
}
